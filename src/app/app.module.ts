import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSliderModule } from '@angular/material/slider';
import { MatCheckboxModule } from '@angular/material/checkbox';

//Material
import {MatExpansionModule} from '@angular/material/expansion';
import { FormsModule }   from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';

 //firebase
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFireStorageModule} from '@angular/fire/storage'; 
import { HttpClientModule } from '@angular/common/http';
// import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { PostsComponent } from './posts/posts.component';
import { SavedpostComponent } from './savedpost/savedpost.component';
import { DocformComponent } from './docform/docform.component';
import { ClassifyComponent } from './classify/classify.component';
import { PostformComponent } from './postform/postform.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { TempformComponent } from './tempform/tempform.component';
import { ItemsComponent } from './items/items.component';
import { TodosComponent } from './todos/todos.component';
import { MytodoComponent } from './mytodo/mytodo.component';
import { ItemsformComponent } from './itemsform/itemsform.component';



const appRoutes: Routes = [ 
  { path: 'welcome', component: WelcomeComponent },
  { path: 'todos', component: TodosComponent },
  { path: 'itemsform', component: ItemsformComponent },
  { path: 'mytodo', component: MytodoComponent },
  { path: 'items', component: ItemsComponent },
  { path: 'temperatures/:city', component: TemperaturesComponent },
  { path: 'tempform', component: TempformComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'allposts', component: PostsComponent },
  { path: 'savepost', component: SavedpostComponent },
  { path: 'classify', component: DocformComponent },
  { path: 'classified', component: ClassifyComponent },
  { path: 'postform', component: PostformComponent},
  { path: 'postform/:id', component: PostformComponent },
  { path: '', redirectTo: '/welcome', pathMatch: 'full'}]; 
  
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignupComponent,
    LoginComponent,
    WelcomeComponent,
    PostsComponent,
    SavedpostComponent,
    DocformComponent,
    ClassifyComponent,
    PostformComponent,
    TemperaturesComponent,
    TempformComponent,
    ItemsComponent,
    TodosComponent,
    MytodoComponent,
    ItemsformComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    FormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatSelectModule,
    MatInputModule,
  RouterModule.forRoot( appRoutes,{ enableTracing: true }),
  AngularFireModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    FormsModule, 
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule, 
    MatSliderModule,
    // CommonModule

  ],
  providers: [AngularFirestore,AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
