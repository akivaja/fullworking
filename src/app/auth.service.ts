import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { User } from './interfaces/user';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<User | null>;
  
  constructor(public afAuth:AngularFireAuth,
              private router:Router) {
    this.user = this.afAuth.authState;}

//הודעת שגיאה
  private logInErrorSubject = new Subject<string>();

  public getLoginErrors():Subject<string>{
    return this.logInErrorSubject;}
 // 

    getUser(){
      return this.user
    }
  
    SignUp(email:string, password:string){
      this.afAuth
          .auth
          .createUserWithEmailAndPassword(email,password)
          .then(res =>{ console.log('Successful Signup',res);
          this.router.navigate(['/welcome']);
        })
          .catch (
            error => window.alert(error))
             // this.router.navigate(['/signup']);
    }
  
    Logout(){
      this.afAuth.auth.signOut();  
      this.router.navigate(['/welcome']);
    }
  
    login(email:string, password:string){
      this.afAuth
          .auth.signInWithEmailAndPassword(email,password)
          .then(res =>{ console.log('Succesful Login',res);
          this.router.navigate(['/welcome']); })
          .catch (
                     error => window.alert(error)
                       ) 
                   
              }
             
  
  }
