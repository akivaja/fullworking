export interface Post {
    userId: number;
    id: number;
    name: string;
    email: string;
    phone: string;
}
