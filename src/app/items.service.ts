import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {
  

  constructor(public db:AngularFirestore) { }
  
  getItems():Observable<any[]>{
    return this.db.collection('items').valueChanges({idField:'id'});
  }

  deleteItem(id:string){
    this.db.doc(`items/${id}`).delete();
  }

  
  changeState(id:string, stock:boolean){
    if(stock){
      this.db.doc(`items/${id}`).update({
        instock:false,
      })
    }
    else{
      this.db.doc(`items/${id}`).update({
        instock:true,
      })
    }


    

  }

  addItem(name:string, price:string, quantity:string){
    const item = {name:name, price:price,quantity:quantity }
    this.db.collection('items').add(item);
  

  }

}
