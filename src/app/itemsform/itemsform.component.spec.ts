import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsformComponent } from './itemsform.component';

describe('ItemsformComponent', () => {
  let component: ItemsformComponent;
  let fixture: ComponentFixture<ItemsformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
