import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../items.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-itemsform',
  templateUrl: './itemsform.component.html',
  styleUrls: ['./itemsform.component.css']
})
export class ItemsformComponent implements OnInit {

  constructor(private itemsservice:ItemsService, 
    private router:Router) { }
name:string;
price:string;
quantity:string;
instock:string;


  ngOnInit(): void {
  }
  onSubmit(){
    this.itemsservice.addItem(this.name, this.price, this.quantity)
    this.router.navigate(['/items']);
  }

}
