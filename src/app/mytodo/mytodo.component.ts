import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { TodosService } from './../todos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mytodo',
  templateUrl: './mytodo.component.html',
  styleUrls: ['./mytodo.component.css']
})
export class MytodoComponent implements OnInit {

  constructor(public todosservice:TodosService,
              public authservice:AuthService,
              public db:AngularFirestore) { }

  todos$:Observable<any[]>;
  userId:string;
  completed:boolean;


  ngOnInit() {
   //this.books$ = this.bookserv.getBooks();
   this.authservice.user.subscribe(
    user => {
      this.userId = user.uid;
      console.log(this.userId);
      this.todos$ = this.todosservice.getTodo(this.userId);
      console.log('there is '+ this.todos$);
     }
  )}

  changeState(id:string, completed:string){
    this.todosservice.changeCheckBox(id, completed, this.userId);
  }

  

}