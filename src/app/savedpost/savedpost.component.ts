import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-savedpost',
  templateUrl: './savedpost.component.html',
  styleUrls: ['./savedpost.component.css']
})
export class SavedpostComponent implements OnInit {

  posts$:Observable<any>;
  userId:string;
  postid:number;
  like:number;
  name:string;
  email:string;
  id:number;
  phone:string;
  constructor(private postService:PostService,public auth:AuthService) { }

  ngOnInit() {
      //this.books$ = this.bookserv.getBooks();
      this.auth.user.subscribe(
        user => {
          this.userId = user.uid;
          console.log(this.userId);
      this.posts$ = this.postService.getpostofuser(this.userId);
         }
      )
  }
 // מוסיפה לייקים
  addlike(id:string,likes:number){
    this.like = likes + 1 ; 
    this.postService.updatepostlike(this.userId,id,this.like)
  }
// מוחקת פוסטים
  deletePost(id:string){
    this.postService.deletePost(this.userId,id)
  }

}
