import { AuthService } from './../auth.service';
import { ActivatedRoute } from '@angular/router';
import { TodosService } from './../todos.service';
import { Component, OnInit } from '@angular/core';
import { Todo } from '../interfaces/todo';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  constructor(public todosservice:TodosService,
              public activatedroutes:ActivatedRoute,
              public authservice:AuthService) { }
  todos$: Todo[];
  //comments$: Comment[];
  title:string;
  id:number;
  //userId:string;
  completed:string;
  userId:string;
  
  ngOnInit() {
    this.todosservice.getTodos().subscribe(
      data => {
        this.todos$ = data;
      }
    )
    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
      }
    )
  }

  addToList(title:string, completed:string){
    console.log(completed)
    console.log(title)
    this.todosservice.saveTodo(this.userId, title, completed)
    }

  

}