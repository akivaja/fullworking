import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  userId:string;
  useremail:string;

  constructor(public auth:AuthService) { }

  ngOnInit(): void {
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.useremail = user.email;
       }
    )
  }

}
