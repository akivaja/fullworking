// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBF1UWGfb0cMLkzm2pI6XZGPg9LWDTHMu8",
    authDomain: "fullworking-90dbe.firebaseapp.com",
    databaseURL: "https://fullworking-90dbe.firebaseio.com",
    projectId: "fullworking-90dbe",
    storageBucket: "fullworking-90dbe.appspot.com",
    messagingSenderId: "351876187653",
    appId: "1:351876187653:web:b9af9f2a8fc8105d26be6c"
}};

/*
 * For efullworkinger debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
